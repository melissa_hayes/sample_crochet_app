require 'test_helper'

class CrochetersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @crocheter = crocheters(:one)
  end

  test "should get index" do
    get crocheters_url
    assert_response :success
  end

  test "should get new" do
    get new_crocheter_url
    assert_response :success
  end

  test "should create crocheter" do
    assert_difference('Crocheter.count') do
      post crocheters_url, params: { crocheter: { description: @crocheter.description, id: @crocheter.id, image_url: @crocheter.image_url, name: @crocheter.name } }
    end

    assert_redirected_to crocheter_url(Crocheter.last)
  end

  test "should show crocheter" do
    get crocheter_url(@crocheter)
    assert_response :success
  end

  test "should get edit" do
    get edit_crocheter_url(@crocheter)
    assert_response :success
  end

  test "should update crocheter" do
    patch crocheter_url(@crocheter), params: { crocheter: { description: @crocheter.description, id: @crocheter.id, image_url: @crocheter.image_url, name: @crocheter.name } }
    assert_redirected_to crocheter_url(@crocheter)
  end

  test "should destroy crocheter" do
    assert_difference('Crocheter.count', -1) do
      delete crocheter_url(@crocheter)
    end

    assert_redirected_to crocheters_url
  end
end
