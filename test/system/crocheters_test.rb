require "application_system_test_case"

class CrochetersTest < ApplicationSystemTestCase
  setup do
    @crocheter = crocheters(:one)
  end

  test "visiting the index" do
    visit crocheters_url
    assert_selector "h1", text: "Crocheters"
  end

  test "creating a Crocheter" do
    visit crocheters_url
    click_on "New Crocheter"

    fill_in "Description", with: @crocheter.description
    fill_in "Id", with: @crocheter.id
    fill_in "Image url", with: @crocheter.image_url
    fill_in "Name", with: @crocheter.name
    click_on "Create Crocheter"

    assert_text "Crocheter was successfully created"
    click_on "Back"
  end

  test "updating a Crocheter" do
    visit crocheters_url
    click_on "Edit", match: :first

    fill_in "Description", with: @crocheter.description
    fill_in "Id", with: @crocheter.id
    fill_in "Image url", with: @crocheter.image_url
    fill_in "Name", with: @crocheter.name
    click_on "Update Crocheter"

    assert_text "Crocheter was successfully updated"
    click_on "Back"
  end

  test "destroying a Crocheter" do
    visit crocheters_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Crocheter was successfully destroyed"
  end
end
