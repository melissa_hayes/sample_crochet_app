require "application_system_test_case"

class YarnsTest < ApplicationSystemTestCase
  setup do
    @yarn = yarns(:one)
  end

  test "visiting the index" do
    visit yarns_url
    assert_selector "h1", text: "Yarns"
  end

  test "creating a Yarn" do
    visit yarns_url
    click_on "New Yarn"

    fill_in "Brand", with: @yarn.brand
    fill_in "Color", with: @yarn.color
    fill_in "Dye lot", with: @yarn.dye_lot
    fill_in "Project", with: @yarn.project_id
    fill_in "Purchased at", with: @yarn.purchased_at
    fill_in "Quantity", with: @yarn.quantity
    click_on "Create Yarn"

    assert_text "Yarn was successfully created"
    click_on "Back"
  end

  test "updating a Yarn" do
    visit yarns_url
    click_on "Edit", match: :first

    fill_in "Brand", with: @yarn.brand
    fill_in "Color", with: @yarn.color
    fill_in "Dye lot", with: @yarn.dye_lot
    fill_in "Project", with: @yarn.project_id
    fill_in "Purchased at", with: @yarn.purchased_at
    fill_in "Quantity", with: @yarn.quantity
    click_on "Update Yarn"

    assert_text "Yarn was successfully updated"
    click_on "Back"
  end

  test "destroying a Yarn" do
    visit yarns_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Yarn was successfully destroyed"
  end
end
