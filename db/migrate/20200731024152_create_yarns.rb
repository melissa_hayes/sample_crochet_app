class CreateYarns < ActiveRecord::Migration[6.0]
  def change
    create_table :yarns do |t|
      t.string :brand
      t.string :color
      t.string :dye_lot
      t.integer :quantity
      t.string :weight
      t.belongs_to :project, null: false, foreign_key: true

      t.timestamps
    end
  end
end
