class CreateProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :projects do |t|
      t.string :pattern
      t.string :hook_size
      t.string :status
      t.belongs_to :crocheter, null: false, foreign_key: true

      t.timestamps
    end
  end
end
