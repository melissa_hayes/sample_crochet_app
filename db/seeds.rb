# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Yarn.delete_all
Project.delete_all 
Crocheter.delete_all
Crocheter.create!(name: "Melissa",description: "Melissa loves to crochet...",image_url: 'profilePic.png')
Project.create!(pattern: "Mandala", hook_size: "I", status: "Complete",crocheter_id: 1)
Yarn.create!(brand: "Red Heart With Love","Bubblegum","56456",3,"light worsted")
Yarn.create!(brand: "Red Heart With Love","Jade","75354",3,"light worsted")
Yarn.create!(brand: "Red Heart With Love","Sunflower","44558",3,"light worsted")
Yarn.create!(brand: "Red Heart With Love","Aqua","58978",3,"light worsted")

Project.create!(pattern: "Small Sunflower", hook_size: "J", status: "In Progress",crocheter_id: 1)
Yarn.create!(brand: "Caron One Pound","Sunflower","5668",1,"Bulky")


Crocheter.create!(name: "Whitney",description: "Whitney is learning to crochet...",image_url: 'whitProfilePic.jpg')
Project.create!(pattern: "Hotpad", hook_size: "J", status: "Complete",crocheter_id: 2)
Yarn.create!(brand: "Mayflower Alpaca Touch","Bordeaux","5668",1,"worsted")
