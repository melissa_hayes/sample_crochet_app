Rails.application.routes.draw do
  # resources :yarns
  # resources :projects

  resources :crocheters do
     resources :projects
  end 	

  resources :project do 
  	 resources :yarn 
  end 
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
