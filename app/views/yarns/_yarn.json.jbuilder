json.extract! yarn, :id, :brand, :color, :dye_lot, :quantity, :weight, :project_id, :created_at, :updated_at
json.url yarn_url(yarn, format: :json)
