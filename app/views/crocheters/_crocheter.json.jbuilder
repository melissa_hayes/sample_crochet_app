json.extract! crocheter, :id, :id, :name, :description, :image_url, :created_at, :updated_at
json.url crocheter_url(crocheter, format: :json)
