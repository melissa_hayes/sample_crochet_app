json.extract! project, :id, :pattern, :hook_size, :status, :crocheter_id, :created_at, :updated_at
json.url project_url(project, format: :json)
