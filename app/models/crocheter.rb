class Crocheter < ApplicationRecord
  has_many :projects
  attr_accessor :project 


# cpcp = Crocheter.includes(:project).find(1)
# => #<Crocheter id: 1, name: "Melissa", description: "Melissa loves to crochet...", image_url: "profilePic.png", created_at: "2020-07-26 21:43:09", updated_at: "2020-07-26 21:43:09">
#  cp.project
# => #<ActiveRecord::Associations::CollectionProxy [#<Project id: 1, pattern: "Mandala", hook_size: "I", status: "Complete", crocheter_id: 1, created_at: "2020-07-26 21:43:09", updated_at: "2020-07-26 21:43:09">, #<Project id: 2, pattern: "Small Sunflower", hook_size: "J", status: "In Progress", crocheter_id: 1, created_at: "2020-07-26 21:43:09", updated_at: "2020-07-26 21:43:09">]>
# irb(main):016:0> cp.project.each do |p|
# irb(main):017:1* puts p
# irb(main):018:1> end
# #<Project:0x00007fff3e6f9628>
# #<Project:0x00007fff3eeb9ed0>
# => [#<Project id: 1, pattern: "Mandala", hook_size: "I", status: "Complete", crocheter_id: 1, created_at: "2020-07-26 21:43:09", updated_at: "2020-07-26 21:43:09">, #<Project id: 2, pattern: "Small Sunflower", hook_size: "J", status: "In Progress", crocheter_id: 1, created_at: "2020-07-26 21:43:09", updated_at: "2020-07-26 21:43:09">]
# irb(main):019:0> 

  def self.crocheter_projects(id)
     crocheter_projects = Crocheter.includes(:project).find(:id)
  end 

end
