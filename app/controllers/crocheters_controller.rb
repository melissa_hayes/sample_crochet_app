class CrochetersController < ApplicationController
  before_action :set_crocheter, only: [:show, :edit, :update, :destroy]

  # GET /crocheters
  # GET /crocheters.json
  def index
    # @crocheters = Crocheter.all
    @crocheters = Crocheter.includes(:projects)
  
  # @products = Product.includes(:product_images)
  #                  .where(product_images: { default_image: true )
  #                  .last(5)

     # @cp ||= Crocheter.crocheter_projects(param[:crocheter_id])

  end

  # GET /crocheters/1
  # GET /crocheters/1.json
  # default is empty def 
  def show
    @crocheters = Crocheter.includes(:patterns)
    
  end
 
    # respond_to do |format| 
    #   format.html
    # end 


  # GET /crocheters/new
  def new
    @crocheter = Crocheter.new
  end

  # GET /crocheters/1/edit
  def edit
  end

  # POST /crocheters
  # POST /crocheters.json
  def create
    @crocheter = Crocheter.new(crocheter_params)

    respond_to do |format|
      if @crocheter.save
        format.html { redirect_to @crocheter, notice: 'Crocheter was successfully created.' }
        format.json { render :show, status: :created, location: @crocheter }
      else
        format.html { render :new }
        format.json { render json: @crocheter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /crocheters/1
  # PATCH/PUT /crocheters/1.json
  def update
    respond_to do |format|
      if @crocheter.update(crocheter_params)
        format.html { redirect_to @crocheter, notice: 'Crocheter was successfully updated.' }
        format.json { render :show, status: :ok, location: @crocheter }
      else
        format.html { render :edit }
        format.json { render json: @crocheter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /crocheters/1
  # DELETE /crocheters/1.json
  def destroy
    @crocheter.destroy
    respond_to do |format|
      format.html { redirect_to crocheters_url, notice: 'Crocheter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_crocheter
      @crocheter = Crocheter.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def crocheter_params
      params.require(:crocheter).permit(:id, :name, :description, :image_url)
    end
end
